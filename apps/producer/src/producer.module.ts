import { Module } from '@nestjs/common';
import { RabbitmqQueueModule } from '../../../libs/shared/src/rabbitmq-queue/rabbitmq-queue.module';
import { ProducerController } from './producer.controller';

@Module({
  imports: [
    RabbitmqQueueModule.register({
      credentials: {
        host: 'localhost',
        password: 'guest',
        port: 5672,
        vhost: 'javel',
        user: 'guest'
      },
      queue: {
        name: 'my-queue',
        deadLetter: {
          exchange: 'dlx',
          patterns: ['SEND_MESSAGE']
        }
      }
    })
  ],
  controllers: [ProducerController],
})
export class ProducerModule { }
